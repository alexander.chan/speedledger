import { combineReducers } from "redux";
import InvoiceReducer from "./reducer_invoice.js";

const rootReducer = combineReducers({
    invoice: InvoiceReducer
});

export default rootReducer;
