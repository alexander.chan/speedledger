
export default function(state = null, action) {
    switch (action.type) {
        case "FETCH_INVOICE":
            return action.payload;
        default:
            return state;
    }
}
