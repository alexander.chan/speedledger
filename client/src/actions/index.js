import axios from "axios";
const ROOT_URL = `http://localhost:8080/api`;

export const fetchInvoice = id => async dispatch => {
    const url = `${ROOT_URL}/getInvoice?id=${id}`;
    const response = await axios.get(url);

    dispatch({ type: "FETCH_INVOICE", payload: response.data });
};
