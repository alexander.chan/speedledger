import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchInvoice } from "../actions";
import "./App.css";
import { Dropdown } from "semantic-ui-react";

let invoiceList = [
    {
        text: "Invoice 1",
        value: 1
    },
    {
        text: "Invoice 2",
        value: 2
    },
    {
        text: "Invoice 3",
        value: 3
    }
];

class App extends Component {
    handleChange = (event, data) => {
        this.props.fetchInvoice(data.value);
    };

    renderTable() {
        if (this.props.invoice) {
            let {
                sender: { name: sendName },
                receiver: { name: resName },
                invoiceDate,
                payBy,
                totalPrice,
                account
            } = this.props.invoice;

            return (
                <table className="ui blue table">
                    <thead>
                        <tr>
                            <th>Sender:</th>
                            <th>Receiver:</th>
                            <th>Invoice date:</th>
                            <th>Pay by:</th>
                            <th>Account:</th>
                            <th>Total price:</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{sendName}</td>
                            <td>{resName}</td>
                            <td>{invoiceDate}</td>
                            <td>{payBy}</td>
                            <td>{account}</td>
                            <td>{totalPrice}</td>
                        </tr>
                    </tbody>
                </table>
            );
        }
    }

    render() {
        return (
            <div className="App">
                <Dropdown
                    onChange={this.handleChange}
                    placeholder="Select Invoice"
                    fluid
                    selection
                    options={invoiceList}
                />
                {this.renderTable()}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { invoice: state.invoice };
};

export default connect(
    mapStateToProps,
    { fetchInvoice }
)(App);
