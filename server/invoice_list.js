const InvoiceList = [
    {
        invoiceId: 1,
        sender: {
            name: "Anders Andersson",
            street: "Storgatan 1",
            city: "Göteborg",
            postalcode: "417 50"
        },

        receiver: {
            name: "SpeedLedger",
            street: "Spannmålsgatan 19",
            city: "Göteborg",
            postalcode: "411 05"
        },
        invoiceDate: "2018-01-01",
        payBy: "2018-02-01",
        lines: [
            {
                qty: 1,
                description: "bookkeeping",
                vat: 25,
                price: 199
            },

            {
                qty: "3",
                description: "invoicing service",
                vat: 5,
                price: 29
            }
        ],
        totalPrice: 286,
        account: "121212-1212"
    },
    {
        invoiceId: 2,
        sender: {
            name: "Björn Baklund",
            street: "Storgatan 1",
            city: "Göteborg",
            postalcode: "417 50"
        },

        receiver: {
            name: "SpeedLedger",
            street: "Spannmålsgatan 19",
            city: "Göteborg",
            postalcode: "411 05"
        },
        invoiceDate: "2018-05-01",
        payBy: "2018-07-12",
        lines: [
            {
                qty: 1,
                description: "bookkeeping",
                vat: 25,
                price: 199
            },

            {
                qty: "3",
                description: "invoicing service",
                vat: 5,
                price: 29
            }
        ],
        totalPrice: 286,
        account: "121212-1212"
    },
    {
        invoiceId: 3,
        sender: {
            name: "Charlotte Chan",
            street: "Storgatan 1",
            city: "Göteborg",
            postalcode: "417 50"
        },

        receiver: {
            name: "SpeedLedger",
            street: "Spannmålsgatan 19",
            city: "Göteborg",
            postalcode: "411 05"
        },
        invoiceDate: "2018-12-01",
        payBy: "2018-12-22",
        lines: [
            {
                qty: 1,
                description: "bookkeeping",
                vat: 25,
                price: 199
            },

            {
                qty: "3",
                description: "invoicing service",
                vat: 5,
                price: 29
            }
        ],
        totalPrice: 286,
        account: "121212-1212"
    }
];

module.exports = {
    InvoiceList: InvoiceList
};
