const express = require("express");
const app = express();
var cors = require('cors');
var InvoiceList = require("./invoice_list.js").InvoiceList;

app.use(cors());
app.get("/api/getInvoice", (req, res) => {
    var invoiceId = parseInt(req.query.id);

    let result = InvoiceList.find(obj => {
        return obj.invoiceId === invoiceId;
    });

    res.json(result);
});
app.listen(8080, () => console.log("Listening on port 8080, how may I serve you?"));
